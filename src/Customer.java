public class Customer {
    byte myByte ;
    short myShort ;
    int myInt ;
    long myLong ;
    float myFloat ;
    double myDouble ;
    boolean myBoolean ;
    char myChar ;
public Customer() {
    myByte = 0 ;
    myShort = 0 ;
    myInt = 5 ;
    myLong = 7 ;
    myFloat = 1.5f ;
    myDouble = 2.3d ;
    myChar = 'a' ;
}
public void showData(){
        System.out.println(this.myByte); // nếu chuyển thuộc tính int thành static int thì mới gọi trực tiếp class Customer dc
        System.out.println(this.myShort);
        System.out.println(this.myInt);
        System.out.println(this.myLong);
        System.out.println(this.myFloat);
        System.out.println(this.myDouble);
        System.out.println(this.myChar);
}


}
